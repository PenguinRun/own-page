import React, { Component } from 'react';
import { Col } from 'reactstrap';

import PropTypes from 'prop-types';

export default class Speech extends Component {
  // constructor(props) {
  //   super(props);
  //   this.state = {
  //     test: "",
  //   };
  // }

  static propTypes = {
    data: PropTypes.object,
  }

  color = () => {
    const colors = ["#ffed95", "#e2f2fd", "#CFEE91"];
    const random = colors[Math.floor(Math.random() * colors.length)];
    return random;
  }

  render() {
    const { data } = this.props;
    return (
      <Col xs="12" sm="6" md="4">
        <div className="blog-card">
          <a href={data.link} target="_new">
            <div className="blog-card-top" style={{ background: this.color() }}>
              <p><i className="fa fa-bookmark-o fa-2x" aria-hidden="true" /></p>
            </div>
            <div className="blog-card-bottom">
              <p className="blog-top-p">{data.title}</p>
              <p className="blog-bottom-p">{data.date}</p>
            </div>
          </a>
        </div>
      </Col>
    );
  }
}
