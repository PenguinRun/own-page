import React, { Component } from 'react';
import { Container, Row, Col } from 'reactstrap';

import BlogData from '../components/BlogData';

export default class Content extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: "",
      item: []
    };
  }

  getBlogData = () => {
    fetch("http://localhost:8008/", {
      method: "GET",
    }).then((response) => {
      if (!response.ok) throw new Error(response.statusText);
      return response.json().then((data) => {
        const newDataArray = [];
        for (let i = 0; i <= 5; i += 1) {
          newDataArray.push(data.result[i]);
        }
        this.setState({
          item: newDataArray
        });
      });
    }).catch(err => err);
  }

  componentDidMount() {
    this.getBlogData();
  }

  render() {
    const blogDatas = this.state.item;
    const blog = blogDatas.map((data, index) => (
      <BlogData
        data={data}
        key={index}
      />
    ));
    return (
      <content>
        <Container>
          <Row>
            <Col xs="12" sm="3">
              <p>hi</p>
            </Col>
            <Col xs="12" sm="9">
              <div className="board">
                <div className="board-img">
                  {""}
                </div>
              </div>
            </Col>
          </Row>
          <p className="class-title">BLOG CONTENTS</p>
          <Row>
            {blog}
          </Row>
          <p className="class-title">BELIEF</p>
          <Row className="row-about">
            <Col xs="12" sm="12" md="12">
              <Row>
                <Col xs="12" sm="3" md="3">
                  <div className="" />
                </Col>
                <Col xs="12" sm="9" md="9">
                  <div className="belief-space" />
                </Col>
              </Row>
            </Col>
            <Col xs="12" sm="12" md="12">
              <Row>
                <Col xs="12" sm="6" md="6">
                  <div className="belief-space-left" />
                  <div className="belief-slogan"><p>We over me. Problem solving with a positive impact.</p></div>
                </Col>
                <Col xs="12" sm="6" md="6">
                  <div className="belief-space-right">
                    <p>test too. </p>
                  </div>
                </Col>
              </Row>
            </Col>
          </Row>
        </Container>
      </content >
    );
  }
}
