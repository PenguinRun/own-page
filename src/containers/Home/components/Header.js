import React, { Component } from 'react';
import { Container, Row, Col } from 'reactstrap';


export default class Header extends Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      isOpen: false
    };
  }
  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }
  render() {
    return (
      <header>
        <Container>
          <Row>
            <Col xs="12" sm="3">
              <div className="site-title">
                <a href="/#">Justin House</a>
              </div>
            </Col>
            <Col xs="12" sm="9">
              <div className="outside-img">
                <div className="">
                  <ul className="menu-item">
                    <li><a href="/#">test</a></li>
                    <li><a href="/#">Resume</a></li>
                  </ul>
                </div>
                <div className="inside-img">
                  {""}
                </div>
              </div>
            </Col>
          </Row>
          <Row>
            <Col xs="12" sm="12">
              <div className="slogan">
                <h1>Writing web code and having fun in the process</h1>
              </div>
            </Col>
          </Row>
        </Container>
      </header>
    );
  }
}
